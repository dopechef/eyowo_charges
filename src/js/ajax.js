/*Remember to use this function name your form anyting_form,
  The response div anything_form_response,
  The button to trigger the submission anything_form_button,
  then onclick of that button submitCall(anything) and watch the magic */

const submitCall = async (div_id) => {
    console.log(div_id);
    let x = div_id;
    let clients = [];
    for (let i = 1; i <= 3;) {
        let arr = $('#' + i).val();
        let arr1 = arr.split('/');
        let result = {
            wallet: arr1[0],
            amount: parseInt(arr1[1]),
            type: arr1[2]
        }
        clients.push(result);
        i++;
    }
    const url = 'http://35.225.92.45/charge';
    // The data we are going to send in our request
    const debtorval = $('#debtor').val();
    let debtorval1 = debtorval.split("/");
    const debtor = {
        wallet: debtorval1[0],
        amount: parseInt(debtorval1[1]),
        type: debtorval1[2]
    }
    const data = {
        name: $('#name').val(),
        platform: $('#platform').val(),
        clients: JSON.stringify(clients),
        debtor: JSON.stringify(debtor)
    }
    // The parameters we are gonna pass to the fetch function
    const fetchData = {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json'
        }, 
    }
    try {
        console.log(fetchData)
        axios.post(url, data)
          .then(function (response) {
            console.log(response);
            $('#genCharge_form_response').html(response.data.data);
          })
          .catch(function (error) {
            console.log(error);
            $('#genCharge_form_response').html(error.data.data);
          });
    }
    catch (e) {
        console.log(e)
    }
};